#include <stddef.h>
#include "ll_cycle.h"

int ll_has_cycle(node *head) {
    /* TODO: Implement ll_has_cycle */
	if (head == NULL || head->next == NULL) {
		return 0;
	}
	node *slow_ptr = head;
	node *fast_ptr = head;
	do {
		if (slow_ptr == NULL || fast_ptr == NULL || fast_ptr->next == NULL) {
			return 0;
		}
		slow_ptr = slow_ptr->next;
		fast_ptr = fast_ptr->next->next;
		if (slow_ptr == fast_ptr) {
			return 1;
		}
	} while (1);
}
